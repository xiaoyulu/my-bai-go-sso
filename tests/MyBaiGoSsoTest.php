<?php
declare(strict_types=1);

namespace LiLei\MyBaiGoSso\tests;

use PHPUnit\Framework\TestCase;
use LiLei\MyBaiGoSso\MyBaiGoSso;

class MyBaiGoSsoTest extends TestCase
{
    // BaiGoSso 创建应用后生成的自增表 ID
    public $app_id = 1;
    // BaiGoSso key
    public $app_key = '19fc30708d1085378996f903e0ed4f61';
    // BaiGoSso 密钥
    public $app_secret = 'sWrDPrZiPEd1Wvvv';
    // BaiGoSso 服务地址 URL
    public $url = 'http://baigo-sso.test';
    // BaiGoSso 服务地址 URI
    public $uri;

    public function testIsWork()
    {
        dump(__CLASS__.'->'.__FUNCTION__);
    }// testIsWork() end

    public function testCheckName()
    {
        dump("检查账户是否存在");
        $username   = "myadmin";
        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $callback   = $myBaiGoSso->setAttribute('uri', 'check_name')->checkName($username);

        dump($callback);
    }// testCheckName() end

    public function testRegister()
    {
        dump("注册测试");
        $username   = "myadmin";
        $password   = "123456";
        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $callback   = $myBaiGoSso->setAttribute('uil', 'check_name')->checkName($username);
        if (1 > $callback['code'])
        {
            dump($callback);
            return null;
        }

        // 注册
        $response = $myBaiGoSso->setAttribute('uri', 'register')->register($username, $password);
        //dump($response);
        $arr      = $myBaiGoSso->handleResponse($response);
        if (1 > $arr['code'])
        {
            dump($arr);
            return null;
        }

        $code           = $myBaiGoSso->decrypt($response['code']);
        $arr['user_id'] = (int)json_decode($code, true)['user_id'];

        dump($arr);
    }// testRegister() end

    public function testLogin()
    {
        dump("测试登录-个人");
        $username   = "myadmin";
        $password   = "123456";
        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $response   = $myBaiGoSso->setAttribute('uri', 'login')->login($username, $password);
        $arr        = $myBaiGoSso->handleResponse($response);

        if (1 > $arr['code'])
        {
            dump($arr);
            return null;
        }

        $code = $myBaiGoSso->decrypt($response['code']);
        $data = json_decode($code, true);

        $arr['user_access_token']  = $data['user_access_token'];
        $arr['user_refresh_token'] = $data['user_refresh_token'];

        dump($arr);
    }// testLogin() end

    public function testResetPassword()
    {
        dump("测试重置密码");
        $user_id     = 1;
        $oldPassword = "123456";
        $newPassword = "654321";
        $myBaiGoSso  = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $response    = $myBaiGoSso->setAttribute('uri', 'reset_password')->resetPassword($user_id, $oldPassword, $newPassword);
        $arr         = $myBaiGoSso->handleResponse($response);

        dump($arr);
    }// testResetPassword() end

    public function testGetUserInfo()
    {
        dump("测试读取用户数据");
        $user_id    = 1;
        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $response   = $myBaiGoSso->setAttribute('uri', 'user_info')->getUserInfo($user_id);
        $arr        = $myBaiGoSso->handleResponse($response);

        if (1 > $arr['code']) return $arr;
        {
            dump($arr);
            return null;
        }

        $code = $myBaiGoSso->decrypt($response['code']);
        $data = json_decode($code, true);
        $arr  = array_merge($arr, $data);

        dump($arr);
    }// testGetUserInfo() end

    public function testSyncLogin()
    {
        dump("测试同步登录");
        $user_id           = 1;
        $user_access_token = "";

        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $response   = $myBaiGoSso->setAttribute('uri', 'sync_login')->syncLogin($user_id, $user_access_token);
        $arr        = $myBaiGoSso->handleResponse($response);

        if (1 > $arr['code'])
        {
            dump($arr);
            return null;
        }
        $arr['urlRows'] = $response['urlRows'];

        dump($arr);
    }// testSyncLogin() end

    public function testSyncLogout()
    {
        dump("测试同步登出");
        $user_id           = 1;
        $user_access_token = "";

        $myBaiGoSso = new MyBaiGoSso($this->app_id, $this->app_key, $this->app_secret, $this->url);
        $response   = $myBaiGoSso->setAttribute('uri', 'sync_logout')->syncLogout($user_id, $user_access_token);
        $arr        = $myBaiGoSso->handleResponse($response);

        if (1 > $arr['code'])
        {
            dump($arr);
            return null;
        }
        $arr['urlRows'] = $response['urlRows'];

        return $arr;
    }// testSyncLogout() end
}