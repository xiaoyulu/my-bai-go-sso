# MyBaiGoSso 扩展包

for bai-go-sso v4.0 alpha-2


## 安装

[lilei/my-bai-go-sso](https://packagist.org/packages/lilei/my-bai-go-sso)

```shell
$ composer require lilei/my-bai-go-sso
```


安装本扩展如果报错：

```
 [InvalidArgumentException]
  Could not find a version of package lilei/my-bai-go-sso matching your minimum-stability (stable). 
  Require it with an explicit version constraint allowing its desired stability.
```

更改 `composer.json`

```
	"require": {},
+   "minimum-stability": "dev"
```



## package

- symfony/var-dumper
- phpunit/phpunit



## 测试

根据 tests/MyBaiGoSsoTest.php 进行调用，将里面的测试参数更新为自己项目里的。

```shell
# 测试指定方法
$ vendor/bin/phpunit tests/MyBaiGoSsoTest.php --filter testIsWork

# 测试全部方法
$ vendor/bin/phpunit tests/MyBaiGoSsoTest.php
```

